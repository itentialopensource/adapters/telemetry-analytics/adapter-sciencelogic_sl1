
## 1.2.4 [10-15-2024]

* Changes made at 2024.10.14_20:19PM

See merge request itentialopensource/adapters/adapter-sciencelogic_sl1!12

---

## 1.2.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-sciencelogic_sl1!10

---

## 1.2.2 [08-14-2024]

* Changes made at 2024.08.14_18:30PM

See merge request itentialopensource/adapters/adapter-sciencelogic_sl1!9

---

## 1.2.1 [08-07-2024]

* Changes made at 2024.08.06_19:43PM

See merge request itentialopensource/adapters/adapter-sciencelogic_sl1!8

---

## 1.2.0 [05-20-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!7

---

## 1.1.3 [03-27-2024]

* Changes made at 2024.03.27_13:14PM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!6

---

## 1.1.2 [03-12-2024]

* Changes made at 2024.03.12_10:58AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!5

---

## 1.1.1 [02-27-2024]

* Changes made at 2024.02.27_11:33AM

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!4

---

## 1.1.0 [01-02-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!3

---

## 1.0.0 [11-29-2022]

* ADAPT-2355 changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!2

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!1

---

## 0.1.1 [10-04-2021]

- Initial Commit

See commit 0061fe5

---
