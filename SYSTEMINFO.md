# ScienceLogic

Vendor: ScienceLogic
Homepage: https://sciencelogic.com/

Product: SL1
Product Page: https://sciencelogic.com/platform/overview

## Introduction
We classify ScienceLogic SL1 into the Service Assurance domain as ScienceLogic SL1 provides information on Events and Monitoring of Networks. 

"See everything across multi-cloud and distributed architectures, contextualize data through relationship mapping, and act on this insight through integration and automation." 

## Why Integrate
The ScienceLogic SL1 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ScienceLogic SL1 to offer a Monitoring, Events and Analytics. 

With this adapter you have the ability to perform operations with ScienceLogic SL1 such as:

- Device
- Event
- Ticket

## Additional Product Documentation
The [ScienceLogic REST API](https://docs.sciencelogic.com/latest/Content/Web_Content_Dev_and_Integration/ScienceLogic_API/api_intro.htm)
The [ScienceLogic REST API Authentication](https://docs.sciencelogic.com/11-2-0/Content/Web_Content_Dev_and_Integration/REST_API_Cookbook/rest_cookbook_gs.htm)
