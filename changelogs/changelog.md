
## 1.0.0 [11-29-2022]

* ADAPT-2355 changes

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!2

---

## 0.2.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/telemetry-analytics/adapter-sciencelogic_sl1!1

---

## 0.1.1 [10-04-2021]

- Initial Commit

See commit 0061fe5

---
