# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the ScienceLogicSl1 System. The API that was used to build the adapter for ScienceLogicSl1 is usually available in the report directory of this adapter. The adapter utilizes the ScienceLogicSl1 API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The ScienceLogic SL1 adapter from Itential is used to integrate the Itential Automation Platform (IAP) with ScienceLogic SL1 to offer a Monitoring, Events and Analytics. 

With this adapter you have the ability to perform operations with ScienceLogic SL1 such as:

- Device
- Event
- Ticket

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
