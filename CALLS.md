## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for ScienceLogic SL1. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for ScienceLogic SL1.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the ScienceLogic. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">getAccountWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of user accounts.</td>
    <td style="padding:15px">{base_path}/{version}/account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccount(body, callback)</td>
    <td style="padding:15px">Create a new user account.</td>
    <td style="padding:15px">{base_path}/{version}/account?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a user account.</td>
    <td style="padding:15px">{base_path}/{version}/account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of a user account.</td>
    <td style="padding:15px">{base_path}/{version}/account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountX(x, body, callback)</td>
    <td style="padding:15px">Replace a user account.</td>
    <td style="padding:15px">{base_path}/{version}/account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountX(x, callback)</td>
    <td style="padding:15px">Delete a user account.</td>
    <td style="padding:15px">{base_path}/{version}/account/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountXAccessHooksWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the list of access hooks that have been granted to a user account.</td>
    <td style="padding:15px">{base_path}/{version}/account/{pathv1}/access_hooks?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessLockWithQuery(query, callback)</td>
    <td style="padding:15px">View a list of locked-out user accounts.</td>
    <td style="padding:15px">{base_path}/{version}/access_lock?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccessLockXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about a locked-out user account.</td>
    <td style="padding:15px">{base_path}/{version}/access_lock/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccessLockX(x, callback)</td>
    <td style="padding:15px">Clear a lock on a user account.</td>
    <td style="padding:15px">{base_path}/{version}/access_lock/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAlert(body, callback)</td>
    <td style="padding:15px">Create a new API alert.</td>
    <td style="padding:15px">{base_path}/{version}/alert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of pending API alerts.</td>
    <td style="padding:15px">{base_path}/{version}/alert?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAlertXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about a pending API alert.</td>
    <td style="padding:15px">{base_path}/{version}/alert/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAlertX(x, body, callback)</td>
    <td style="padding:15px">Update a pending API alert.</td>
    <td style="padding:15px">{base_path}/{version}/alert/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SL1 appliances.</td>
    <td style="padding:15px">{base_path}/{version}/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a SL1 appliance.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplianceX(x, body, callback)</td>
    <td style="padding:15px">Update the description or IP address of a SL1 appliance.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of asset records.</td>
    <td style="padding:15px">{base_path}/{version}/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAsset(body, callback)</td>
    <td style="padding:15px">Create a new asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the general properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetX(x, body, callback)</td>
    <td style="padding:15px">Replace an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetX(x, body, callback)</td>
    <td style="padding:15px">Update the general properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssetX(x, callback)</td>
    <td style="padding:15px">Delete an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXComponentWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of components associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/component/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXComponent(x, body, callback)</td>
    <td style="padding:15px">Add a new component to an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/component/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXComponentYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a component associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/component/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXComponentY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a component associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/component/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetXComponentY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a component associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/component/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssetXComponentY(x, y, callback)</td>
    <td style="padding:15px">Delete a component from an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/component/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXConfigurationWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the configuration properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/configuration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXConfiguration(x, body, callback)</td>
    <td style="padding:15px">Update the configuration properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/configuration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetXConfiguration(x, body, callback)</td>
    <td style="padding:15px">Replace the configuration properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/configuration/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXLicenseWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of software licenses associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/license/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXLicense(x, body, callback)</td>
    <td style="padding:15px">Add a new software license to an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/license/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXLicenseYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a software license associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/license/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXLicenseY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a software license associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/license/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetXLicenseY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a software license associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/license/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssetXLicenseY(x, y, callback)</td>
    <td style="padding:15px">Delete a software license from an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/license/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXMaintenanceWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the maintenance and service properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXMaintenance(x, body, callback)</td>
    <td style="padding:15px">Update the maintenance and service properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetXMaintenance(x, body, callback)</td>
    <td style="padding:15px">Replace the maintenance and service properties of an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/maintenance/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXNetworkWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of IP networks associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/network/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXNetwork(x, body, callback)</td>
    <td style="padding:15px">Add a new IP network to an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/network/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXNetworkYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of an IP network associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/network/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXNetworkY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of an IP network associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/network/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetXNetworkY(x, y, body, callback)</td>
    <td style="padding:15px">Replace an IP network associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/network/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAssetXNetworkY(x, y, callback)</td>
    <td style="padding:15px">Delete an IP network from an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/network/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXNoteWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of notes associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXNote(x, body, callback)</td>
    <td style="padding:15px">Add a note to an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXNoteYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a note associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAssetXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Update a note associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a note associated with an asset record.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXNoteYMediaWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of files associated with an asset record note.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/{pathv2}/media?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXNoteYMediaZWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">Get a media file associated with an asset record note.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAssetXNoteYMediaZ(x, y, z, body, callback)</td>
    <td style="padding:15px">Add a media file to an asset record note.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssetXNoteYMediaZInfoWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">View meta-data about a media file associated with an asset record note.</td>
    <td style="padding:15px">{base_path}/{version}/asset/{pathv1}/note/{pathv2}/media/{pathv3}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCbqosMetricWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of CBQoS metrics.</td>
    <td style="padding:15px">{base_path}/{version}/cbqos_metric?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCbqosMetricXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about a CBQoS metric.</td>
    <td style="padding:15px">{base_path}/{version}/cbqos_metric/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCbqosObjectWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of CBQoS objects.</td>
    <td style="padding:15px">{base_path}/{version}/cbqos_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCbqosObjectXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about a CBQoS object.</td>
    <td style="padding:15px">{base_path}/{version}/cbqos_object/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCbqosTypeWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of CBQoS object types.</td>
    <td style="padding:15px">{base_path}/{version}/cbqos_type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCbqosTypeXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about a CBQoS object type.</td>
    <td style="padding:15px">{base_path}/{version}/cbqos_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClearedEventWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of cleared events.</td>
    <td style="padding:15px">{base_path}/{version}/cleared_event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getClearedEventXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a cleared event.</td>
    <td style="padding:15px">{base_path}/{version}/cleared_event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectionLabelWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection labels.</td>
    <td style="padding:15px">{base_path}/{version}/collection_label?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectionLabelXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a collection label.</td>
    <td style="padding:15px">{base_path}/{version}/collection_label/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectionLabelGroupWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection label groups.</td>
    <td style="padding:15px">{base_path}/{version}/collection_label_group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectionLabelGroupXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a collection label group.</td>
    <td style="padding:15px">{base_path}/{version}/collection_label_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorGroupWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collector groups.</td>
    <td style="padding:15px">{base_path}/{version}/collector_group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCollectorGroup(body, callback)</td>
    <td style="padding:15px">Create a new collector group.</td>
    <td style="padding:15px">{base_path}/{version}/collector_group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCollectorGroupX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of a collector group.</td>
    <td style="padding:15px">{base_path}/{version}/collector_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCollectorGroupXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a collector group.</td>
    <td style="padding:15px">{base_path}/{version}/collector_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialBasic(body, callback)</td>
    <td style="padding:15px">Create a new basic/snippet credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/basic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCollectorGroupX(x, body, callback)</td>
    <td style="padding:15px">Replace a collector group.</td>
    <td style="padding:15px">{base_path}/{version}/collector_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCollectorGroupX(x, callback)</td>
    <td style="padding:15px">Delete a collector group.</td>
    <td style="padding:15px">{base_path}/{version}/collector_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available credential resources.</td>
    <td style="padding:15px">{base_path}/{version}/credential?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialBasicWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of basic/snippet credentials.</td>
    <td style="padding:15px">{base_path}/{version}/credential/basic?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialBasicXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a basic/snippet credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/basic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialBasicX(x, body, callback)</td>
    <td style="padding:15px">Update a basic/snippet credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/basic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCredentialBasicX(x, body, callback)</td>
    <td style="padding:15px">Replace a basic/snippet credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/basic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialBasicX(x, callback)</td>
    <td style="padding:15px">Delete a basic/snippet credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/basic/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialDbWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of database credentials.</td>
    <td style="padding:15px">{base_path}/{version}/credential/db?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialDb(body, callback)</td>
    <td style="padding:15px">Create a new database credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/db?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialDbXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a database credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/db/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialDbX(x, body, callback)</td>
    <td style="padding:15px">Update a database credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/db/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCredentialDbX(x, body, callback)</td>
    <td style="padding:15px">Replace a database credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/db/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialDbX(x, callback)</td>
    <td style="padding:15px">Delete a database credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/db/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialLdapWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of LDAP/AD credentials.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ldap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialLdap(body, callback)</td>
    <td style="padding:15px">Create a new LDAP/AD credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ldap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialLdapXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a LDAP/AD credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialLdapX(x, body, callback)</td>
    <td style="padding:15px">Update a LDAP/AD credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCredentialLdapX(x, body, callback)</td>
    <td style="padding:15px">Replace a LDAP/AD credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialLdapX(x, callback)</td>
    <td style="padding:15px">Delete a LDAP/AD credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ldap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialPowershellWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of PowerShell credentials.</td>
    <td style="padding:15px">{base_path}/{version}/credential/powershell?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialPowershell(body, callback)</td>
    <td style="padding:15px">Create a new PowerShell credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/powershell?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialPowershellXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a PowerShell credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/powershell/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialPowershellX(x, body, callback)</td>
    <td style="padding:15px">Update a PowerShell credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/powershell/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCredentialPowershellX(x, body, callback)</td>
    <td style="padding:15px">Replace a PowerShell credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/powershell/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialPowershellX(x, callback)</td>
    <td style="padding:15px">Delete a PowerShell credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/powershell/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialSnmpWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SNMP credentials.</td>
    <td style="padding:15px">{base_path}/{version}/credential/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialSnmp(body, callback)</td>
    <td style="padding:15px">Create a new SNMP credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/snmp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialSnmpXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View an SNMP credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/snmp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialSnmpX(x, body, callback)</td>
    <td style="padding:15px">Update an SNMP credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/snmp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCredentialSnmpX(x, body, callback)</td>
    <td style="padding:15px">Replace an SNMP credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/snmp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialSnmpX(x, callback)</td>
    <td style="padding:15px">Delete an SNMP credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/snmp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialSoapWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SOAP/XML credentials.</td>
    <td style="padding:15px">{base_path}/{version}/credential/soap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialSoap(body, callback)</td>
    <td style="padding:15px">Create a new SOAP/XML credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/soap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialSoapXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a SOAP/XML credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/soap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialSoapX(x, body, callback)</td>
    <td style="padding:15px">Update a SOAP/XML credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/soap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCredentialSoapX(x, body, callback)</td>
    <td style="padding:15px">Replace a SOAP/XML credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/soap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialSoapX(x, callback)</td>
    <td style="padding:15px">Delete a SOAP/XML credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/soap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialSshWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SSH credentials.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialSsh(body, callback)</td>
    <td style="padding:15px">Create a new SSH credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ssh?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCredentialSshXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View an SSH credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ssh/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCredentialSshX(x, body, callback)</td>
    <td style="padding:15px">Update an SSH credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ssh/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putCredentialSshX(x, body, callback)</td>
    <td style="padding:15px">Replace an SSH credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ssh/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCredentialSshX(x, callback)</td>
    <td style="padding:15px">Delete an SSH credential.</td>
    <td style="padding:15px">{base_path}/{version}/credential/ssh/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available custom attribute resources.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeAssetWithQuery(query, callback)</td>
    <td style="padding:15px">View the custom attributes defined for assets.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeAsset(body, callback)</td>
    <td style="padding:15px">Add a custom attribute for assets.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeAssetXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details of a custom attribute defined for assets.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeAssetX(x, body, callback)</td>
    <td style="padding:15px">Update a custom attribute defined for assets.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAttributeAssetX(x, callback)</td>
    <td style="padding:15px">Delete a custom attribute defined for assets.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeAssetExampleWithQuery(query, callback)</td>
    <td style="padding:15px">View example JSON or XML content for creating custom attributes for assets.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/asset/_example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeDeviceWithQuery(query, callback)</td>
    <td style="padding:15px">View the custom attributes defined for devices.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeDevice(body, callback)</td>
    <td style="padding:15px">Add a custom attribute for devices.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeDeviceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details of a custom attribute defined for devices.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeDeviceX(x, body, callback)</td>
    <td style="padding:15px">Update a custom attribute defined for devices.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAttributeDeviceX(x, callback)</td>
    <td style="padding:15px">Delete a custom attribute defined for devices.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeDeviceExampleWithQuery(query, callback)</td>
    <td style="padding:15px">View example JSON or XML content for creating custom attributes for devices.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/device/_example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeInterfaceWithQuery(query, callback)</td>
    <td style="padding:15px">View the custom attributes defined for interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeInterface(body, callback)</td>
    <td style="padding:15px">Add a custom attribute for interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeInterfaceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details of a custom attribute defined for interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeInterfaceX(x, body, callback)</td>
    <td style="padding:15px">Update a custom attribute defined for interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAttributeInterfaceX(x, callback)</td>
    <td style="padding:15px">Delete a custom attribute defined for interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeInterfaceExampleWithQuery(query, callback)</td>
    <td style="padding:15px">View example JSON or XML content for creating custom attributes for interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/interface/_example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeThemeWithQuery(query, callback)</td>
    <td style="padding:15px">View the custom attributes defined for themes.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/theme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeTheme(body, callback)</td>
    <td style="padding:15px">Add a custom attribute for themes.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/theme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeThemeXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details of a custom attribute defined for themes.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/theme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeThemeX(x, body, callback)</td>
    <td style="padding:15px">Update a custom attribute defined for themes.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/theme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAttributeThemeX(x, callback)</td>
    <td style="padding:15px">Delete a custom attribute defined for themes.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/theme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeThemeExampleWithQuery(query, callback)</td>
    <td style="padding:15px">View example JSON or XML content for creating custom attributes for themes.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/theme/_example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeVendorWithQuery(query, callback)</td>
    <td style="padding:15px">View the custom attributes defined for vendors.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeVendor(body, callback)</td>
    <td style="padding:15px">Add a custom attribute for vendors.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeVendorXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details of a custom attribute defined for vendors.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/vendor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postCustomAttributeVendorX(x, body, callback)</td>
    <td style="padding:15px">Update a custom attribute defined for vendors.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/vendor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteCustomAttributeVendorX(x, callback)</td>
    <td style="padding:15px">Delete a custom attribute defined for vendors.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/vendor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeVendorExampleWithQuery(query, callback)</td>
    <td style="padding:15px">View example JSON or XML content for creating custom attributes for vendors.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/vendor/_example?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getCustomAttributeLookupWithQuery(query, callback)</td>
    <td style="padding:15px">View the custom attributes defined for all entity types.</td>
    <td style="padding:15px">{base_path}/{version}/custom_attribute/_lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of dashboards.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDashboard(body, callback)</td>
    <td style="padding:15px">Create a new dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDashboardX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDashboardX(x, body, callback)</td>
    <td style="padding:15px">Replace a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDashboardX(x, callback)</td>
    <td style="padding:15px">Delete a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardXWidgetWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of widgets on a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/widget?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDashboardXWidgetYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a widget on a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/widget/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDashboardXWidgetY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a widget on a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/widget/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDashboardXWidgetY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a widget on a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/widget/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDashboardXWidgetY(x, y, callback)</td>
    <td style="padding:15px">Remove a widget from a dashboard.</td>
    <td style="padding:15px">{base_path}/{version}/dashboard/{pathv1}/widget/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSLDeviceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of devices.</td>
    <td style="padding:15px">{base_path}/{version}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDevice(body, callback)</td>
    <td style="padding:15px">Create a new virtual device.</td>
    <td style="padding:15px">{base_path}/{version}/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceX(x, body, callback)</td>
    <td style="padding:15px">Replace the properties of a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceX(x, callback)</td>
    <td style="padding:15px">Delete a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXAlignedAppWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Dynamic Applications aligned with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/aligned_app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceXAlignedApp(x, body, callback)</td>
    <td style="padding:15px">Align a Dynamic Application with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/aligned_app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXAlignedAppYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the collection status and associated credential for a Dynamic Application aligned with a devic</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/aligned_app/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceXAlignedAppY(x, y, body, callback)</td>
    <td style="padding:15px">Update the collection status and associated credential for a Dynamic Application aligned with a dev</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/aligned_app/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceXAlignedAppY(x, y, callback)</td>
    <td style="padding:15px">Unalign a Dynamic Application from a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/aligned_app/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXConfigDataWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of available configuration data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/config_data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXConfigDataYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View meta-data about data collected from a device by a configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/config_data/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXConfigDataYDataWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View data collected from a device by a configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/config_data/{pathv2}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXConfigDataYSnapshotsWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View historical snapshots of data collected from a device by a configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/config_data/{pathv2}/snapshots?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXDetailWithQuery(x, query, callback)</td>
    <td style="padding:15px">View general information collected from a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/detail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXDeviceAppCredentialsWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of credentials aligned with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/device_app_credentials?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXDeviceThresholdsWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the threshold settings for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/device_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceXDeviceThresholds(x, body, callback)</td>
    <td style="padding:15px">Update the threshold settings for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/device_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceXDeviceThresholds(x, body, callback)</td>
    <td style="padding:15px">Replace the threshold settings for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/device_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceXDeviceThresholds(x, callback)</td>
    <td style="padding:15px">Revert all device thresholds to the global default values.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/device_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceXInterface(x, body, callback)</td>
    <td style="padding:15px">Add an interface record to a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXInterfaceWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of interfaces for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXInterfaceYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of an interface for a device, including all interface tags.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceXInterfaceY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of an interface for a device. This can create a new interface without an inte</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceXInterfaceY(x, y, body, callback)</td>
    <td style="padding:15px">Replace an interface record associated with a device. This can update an interface without affectin</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceXInterfaceY(x, y, callback)</td>
    <td style="padding:15px">Delete an interface record associated with a device. Deleting an interface also deletes the interfa</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXInterfaceYInterfaceDataDataWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View data for an interface.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface/{pathv2}/interface_data/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXInterfaceYInterfaceDataNormalizedDailyWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View daily normalized data for an interface.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface/{pathv2}/interface_data/normalized_daily?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXInterfaceYInterfaceDataNormalizedHourlyWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View hourly normalized data for an interface.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/interface/{pathv2}/interface_data/normalized_hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXLogWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of logs associated with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/log/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXLogYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a log associated with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/log/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceXNote(x, body, callback)</td>
    <td style="padding:15px">Add a note to a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXNoteWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of notes associated with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXNoteYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a note associated with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Update a note associated with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a note associated with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceXNoteY(x, y, callback)</td>
    <td style="padding:15px">Delete a note associated with a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXNoteYMediaWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of files associated with a device note.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}/media?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXNoteYMediaZWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">Get a media file associated with a device note.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceXNoteYMediaZ(x, y, z, body, callback)</td>
    <td style="padding:15px">Add a media file to a device note.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXNoteYMediaZInfoWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">View meta-data about a media file associated with a device note.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/note/{pathv2}/media/{pathv3}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXPerformanceDataWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of available Dynamic Application data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/performance_data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXPerformanceDataYDataWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View data for a Dynamic Application aligned to a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/performance_data/{pathv2}/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXPerformanceDataYNormalizedDailyWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View daily normalized data for a Dynamic Application aligned to a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/performance_data/{pathv2}/normalized_daily?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXPerformanceDataYNormalizedHourlyWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View hourly normalized data for a Dynamic Application aligned to a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/performance_data/{pathv2}/normalized_hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of available vitals data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsAvailabilityDataWithQuery(x, query, callback)</td>
    <td style="padding:15px">View availability data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/availability/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsAvailabilityNormalizedDailyWithQuery(x, query, callback)</td>
    <td style="padding:15px">View daily normalized availability data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/availability/normalized_daily?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsAvailabilityNormalizedHourlyWithQuery(x, query, callback)</td>
    <td style="padding:15px">View hourly normalized availability data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/availability/normalized_hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsFsXDataWithQuery(x, query, callback)</td>
    <td style="padding:15px">View data for a file system on a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/fsX/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsFsXNormalizedDailyWithQuery(x, query, callback)</td>
    <td style="padding:15px">View daily normalized data for a file system on a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/fsX/normalized_daily?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsLatencyDataWithQuery(x, query, callback)</td>
    <td style="padding:15px">View latency data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/latency/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsLatencyNormalizedDailyWithQuery(x, query, callback)</td>
    <td style="padding:15px">View daily normalized latency data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/latency/normalized_daily?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceXVitalsLatencyNormalizedHourlyWithQuery(x, query, callback)</td>
    <td style="padding:15px">View hourly normalized latency data for a device.</td>
    <td style="padding:15px">{base_path}/{version}/device/{pathv1}/vitals/latency/normalized_hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCategoryWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of device categories.</td>
    <td style="padding:15px">{base_path}/{version}/device_category?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceCategoryXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a device category.</td>
    <td style="padding:15px">{base_path}/{version}/device_category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceClassWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of device classes.</td>
    <td style="padding:15px">{base_path}/{version}/device_class?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceClassXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a device class.</td>
    <td style="padding:15px">{base_path}/{version}/device_class/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of device groups.</td>
    <td style="padding:15px">{base_path}/{version}/device_group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceGroup(body, callback)</td>
    <td style="padding:15px">Create a new device group.</td>
    <td style="padding:15px">{base_path}/{version}/device_group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a device group.</td>
    <td style="padding:15px">{base_path}/{version}/device_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceGroupX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of a device group.</td>
    <td style="padding:15px">{base_path}/{version}/device_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceGroupX(x, body, callback)</td>
    <td style="padding:15px">Replace a device group.</td>
    <td style="padding:15px">{base_path}/{version}/device_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceGroupX(x, callback)</td>
    <td style="padding:15px">Delete a device group.</td>
    <td style="padding:15px">{base_path}/{version}/device_group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceGroupXExpandedDevicesWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a list of all devices in the device group, including devices that match dynamic rules.</td>
    <td style="padding:15px">{base_path}/{version}/device_group/{pathv1}/expanded_devices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelationshipWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of device relationships.</td>
    <td style="padding:15px">{base_path}/{version}/relationship?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelationshipXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a device relationship.</td>
    <td style="padding:15px">{base_path}/{version}/relationship/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelationshipHierarchyXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of ancestor and decendant devices of a device.</td>
    <td style="padding:15px">{base_path}/{version}/relationship_hierarchy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelationshipTypeWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of device relationship types.</td>
    <td style="padding:15px">{base_path}/{version}/relationship_type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRelationshipTypeXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a device relationship type.</td>
    <td style="padding:15px">{base_path}/{version}/relationship_type/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of device templates.</td>
    <td style="padding:15px">{base_path}/{version}/device_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplate(body, callback)</td>
    <td style="padding:15px">Create a new device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceTemplateX(x, body, callback)</td>
    <td style="padding:15px">Replace a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTemplateX(x, callback)</td>
    <td style="padding:15px">Delete a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplCvWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of web content monitoring policy sub-templates associated with a device</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_cv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplCv(x, body, callback)</td>
    <td style="padding:15px">Create a new web content monitoring policy sub-template for a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_cv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplCvYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a web content monitoring policy sub-template associated with a device templa</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_cv/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplCvY(x, y, body, callback)</td>
    <td style="padding:15px">Update a web content monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_cv/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceTemplateXSubtplCvY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a web content monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_cv/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTemplateXSubtplCvY(x, y, callback)</td>
    <td style="padding:15px">Delete a web content monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_cv/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplDynappWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Dynamic Application sub-templates associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_dynapp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplDynapp(x, body, callback)</td>
    <td style="padding:15px">Create a new Dynamic Application sub-template for a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_dynapp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplDynappYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a Dynamic Application sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_dynapp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplDynappY(x, y, body, callback)</td>
    <td style="padding:15px">Update a Dynamic Application sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_dynapp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceTemplateXSubtplDynappY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a Dynamic Application sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_dynapp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTemplateXSubtplDynappY(x, y, callback)</td>
    <td style="padding:15px">Delete a Dynamic Application sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_dynapp/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplPortWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of port monitoring policy sub-templates associated with a device templa</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplPort(x, body, callback)</td>
    <td style="padding:15px">Create a new port monitoring policy sub-template for a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplPortYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a port monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_port/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplPortY(x, y, body, callback)</td>
    <td style="padding:15px">Update a port monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_port/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceTemplateXSubtplPortY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a port monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_port/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTemplateXSubtplPortY(x, y, callback)</td>
    <td style="padding:15px">Delete a port monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_port/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplProcessWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of process monitoring policy sub-templates associated with a device tem</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_process?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplProcess(x, body, callback)</td>
    <td style="padding:15px">Create a new process monitoring policy sub-template for a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_process?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplProcessYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a process monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_process/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplProcessY(x, y, body, callback)</td>
    <td style="padding:15px">Update a process monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_process/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceTemplateXSubtplProcessY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a process monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_process/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTemplateXSubtplProcessY(x, y, callback)</td>
    <td style="padding:15px">Delete a process monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_process/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplServiceWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Windows service monitoring policy sub-templates associated with a de</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplService(x, body, callback)</td>
    <td style="padding:15px">Create a new Windows service monitoring policy sub-template for a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDeviceTemplateXSubtplServiceYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a Windows service monitoring policy sub-template associated with a device te</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDeviceTemplateXSubtplServiceY(x, y, body, callback)</td>
    <td style="padding:15px">Update a Windows service monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDeviceTemplateXSubtplServiceY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a Windows service monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDeviceTemplateXSubtplServiceY(x, y, callback)</td>
    <td style="padding:15px">Delete a Windows service monitoring policy sub-template associated with a device template.</td>
    <td style="padding:15px">{base_path}/{version}/device_template/{pathv1}/subtpl_service/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of discovery sessions.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoverySession(body, callback)</td>
    <td style="padding:15px">Create a new discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoverySessionX(x, body, callback)</td>
    <td style="padding:15px">Update a discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDiscoverySessionX(x, body, callback)</td>
    <td style="padding:15px">Replace a discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDiscoverySessionX(x, callback)</td>
    <td style="padding:15px">Delete a discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionXLogWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of logs associated with a discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session/{pathv1}/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionXLogYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a log message associated with a discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session/{pathv1}/log/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionActiveWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of currently running discovery sessions.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session_active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDiscoverySessionActive(body, callback)</td>
    <td style="padding:15px">Create and immediately run a new discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session_active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionActiveXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a currently running discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session_active/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDiscoverySessionActiveX(x, callback)</td>
    <td style="padding:15px">Stop a currently running discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session_active/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionActiveXLogWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of logs associated with a currently running discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session_active/{pathv1}/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoverySessionActiveXLogYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a log message associated with a currently running discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/discovery_session_active/{pathv1}/log/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available Dynamic Application resources.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Database Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a Database Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a Database Configuration Dynamic</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppDbConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a Database Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a Database Configuration Dynamic Applica</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppDbConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a Database Configuration Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppDbConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a Database Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppDbConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a Database Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Database Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a Database Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a Database Performance Dynamic Ap</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppDbPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a Database Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppDbPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to a Database Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a Database Performance Dynamic Applicati</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppDbPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a Database Performance Dynamic Applica</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppDbPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a Database Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppDbPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a Database Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with a Database Performance Dynamic</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppDbPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with a Database Performance Dynamic Applica</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppDbPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with a Database Performance Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppDbPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with a Database Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppDbPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from a Database Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/db_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of PowerShell Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a PowerShell Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a PowerShell Configuration Dynami</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppPowershellConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a PowerShell Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a PowerShell Configuration Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppPowershellConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a PowerShell Configuration Dynamic App</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppPowershellConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a PowerShell Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppPowershellConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a PowerShell Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of PowerShell Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a PowerShell Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a PowerShell Performance Dynamic</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppPowershellPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a PowerShell Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a PowerShell Performance Dynamic Applica</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppPowershellPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a PowerShell Performance Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppPowershellPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a PowerShell Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppPowershellPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a PowerShell Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with a PowerShell Performance Dynami</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppPowershellPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to a PowerShell Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppPowershellPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with a PowerShell Performance Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppPowershellPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with a PowerShell Performance Dynamic App</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppPowershellPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with a PowerShell Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppPowershellPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from a PowerShell Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/powershell_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Snippet Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a Snippet Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a Snippet Configuration Dynamic A</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a Snippet Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a Snippet Configuration Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a Snippet Configuration Dynamic Applic</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnippetConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a Snippet Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnippetConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a Snippet Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetJournalWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Snippet Journal Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetJournalXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetJournalXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a Snippet Journal Dynamic Applica</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetJournalXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetJournalXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetJournalXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnippetJournalXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnippetJournalXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetJournalXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetJournalXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with a Snippet Journal Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetJournalXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetJournalXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with a Snippet Journal Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnippetJournalXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnippetJournalXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from a Snippet Journal Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_journal/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Snippet Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a Snippet Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a Snippet Performance Dynamic App</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a Snippet Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a Snippet Performance Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a Snippet Performance Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnippetPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a Snippet Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnippetPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a Snippet Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with a Snippet Performance Dynamic A</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to a Snippet Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnippetPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with a Snippet Performance Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnippetPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with a Snippet Performance Dynamic Applic</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnippetPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with a Snippet Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnippetPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from a Snippet Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snippet_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SNMP Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an SNMP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with an SNMP Configuration Dynamic App</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnmpConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to an SNMP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with an SNMP Configuration Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnmpConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with an SNMP Configuration Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnmpConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with an SNMP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnmpConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from an SNMP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SNMP Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with an SNMP Performance Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnmpPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnmpPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with an SNMP Performance Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnmpPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnmpPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with an SNMP Performance Dynamic App</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnmpPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSnmpPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with an SNMP Performance Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSnmpPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with an SNMP Performance Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSnmpPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSnmpPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from an SNMP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/snmp_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SOAP Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a SOAP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSoapConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a SOAP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a SOAP Configuration Dynamic Appl</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a SOAP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSoapConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a SOAP Configuration Dynamic Applicati</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSoapConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a SOAP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSoapConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a SOAP Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SOAP Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a SOAP Performance Dynamic Applic</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSoapPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSoapPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSoapPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSoapPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with a SOAP Performance Dynamic Appl</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSoapPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppSoapPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppSoapPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with a SOAP Performance Dynamic Applicati</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppSoapPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppSoapPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from a SOAP Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/soap_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of WMI Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a WMI Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a  WMI Configuration Dynamic Appl</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppWmiConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a WMI Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a WMI Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppWmiConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a WMI Configuration Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppWmiConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a WMI Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppWmiConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a WMI Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of WMI Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with a WMI Performance Dynamic Applica</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppWmiPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppWmiPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppWmiPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppWmiPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with a WMI Performance Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppWmiPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppWmiPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppWmiPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with a WMI Performance Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppWmiPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppWmiPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from a WMI Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/wmi_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of XML Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an XML Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXmlConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to an XML Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with an XML Configuration Dynamic Appl</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with an XML Configuration  Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXmlConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with an XML Configuration  Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppXmlConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with an XML Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppXmlConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from an XML Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of XML Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with an XML Performance Dynamic Applic</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXmlPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXmlPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppXmlPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppXmlPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with an XML Performance Dynamic Appl</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXmlPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXmlPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXmlPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with an XML Performance Dynamic Applicati</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppXmlPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppXmlPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from an XML Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xml_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltConfigWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of XSLT Configuration Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltConfigXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an XSLT Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltConfigXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with an XSLT Configuration Dynamic App</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXsltConfigXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to an XSLT Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltConfigXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with an XSLT Configuration Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXsltConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with an XSLT Configuration Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppXsltConfigXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with a  Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppXsltConfigXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from an XSLT Configuration Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_config/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of XSLT Performance Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltPerformanceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltPerformanceXCollectionObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of collection objects associated with an XSLT Performance Dynamic Appli</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXsltPerformanceXCollectionObject(x, body, callback)</td>
    <td style="padding:15px">Add a collection object to an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/collection_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltPerformanceXCollectionObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a collection object associated with an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXsltPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a collection object associated with an XSLT Performance Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppXsltPerformanceXCollectionObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a collection object associated with an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppXsltPerformanceXCollectionObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a collection object from an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/collection_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltPerformanceXPresentationObjectWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of presentation objects associated with an XSLT Performance Dynamic App</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXsltPerformanceXPresentationObject(x, body, callback)</td>
    <td style="padding:15px">Add a presentation object to an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/presentation_object?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppXsltPerformanceXPresentationObjectYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View the properties of a presentation object associated with an XSLT Performance Dynamic Applicatio</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postDynamicAppXsltPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Update the properties of a presentation object associated with an XSLT Performance Dynamic Applicat</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putDynamicAppXsltPerformanceXPresentationObjectY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a presentation object associated with an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDynamicAppXsltPerformanceXPresentationObjectY(x, y, callback)</td>
    <td style="padding:15px">Remove a presentation object from an XSLT Performance Dynamic Application.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/xslt_performance/{pathv1}/presentation_object/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicAppLookupWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of all Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/dynamic_app/_lookup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of active events.</td>
    <td style="padding:15px">{base_path}/{version}/event?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View an active event.</td>
    <td style="padding:15px">{base_path}/{version}/event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEventX(x, callback)</td>
    <td style="padding:15px">Clear an active event.</td>
    <td style="padding:15px">{base_path}/{version}/event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEventX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of an event.</td>
    <td style="padding:15px">{base_path}/{version}/event/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postEventCategoryX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of an event.</td>
    <td style="padding:15px">{base_path}/{version}/event_category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getEventCategoryXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the event category for an active event.</td>
    <td style="padding:15px">{base_path}/{version}/event_category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteEventCategoryX(x, callback)</td>
    <td style="padding:15px">Delete the event category for an active event.</td>
    <td style="padding:15px">{base_path}/{version}/event_category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactsWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of external contacts.</td>
    <td style="padding:15px">{base_path}/{version}/contacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContacts(body, callback)</td>
    <td style="padding:15px">Create a new external contact.</td>
    <td style="padding:15px">{base_path}/{version}/contacts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getContactsXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an external contact.</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postContactsX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of an external contact.</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putContactsX(x, body, callback)</td>
    <td style="padding:15px">Replace an external contact.</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteContactsX(x, callback)</td>
    <td style="padding:15px">Delete an external contact.</td>
    <td style="padding:15px">{base_path}/{version}/contacts/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilestoreWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available filestore resources.</td>
    <td style="padding:15px">{base_path}/{version}/filestore?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilestorePowerpackWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available PowerPack file resources. This index does not include PowerPacks that a</td>
    <td style="padding:15px">{base_path}/{version}/filestore/powerpack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilestorePowerpackXWithQuery(x, query, callback)</td>
    <td style="padding:15px">Download a PowerPack file.</td>
    <td style="padding:15px">{base_path}/{version}/filestore/powerpack/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilestorePowerpackXInfoWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the information associated with a PowerPack file.</td>
    <td style="padding:15px">{base_path}/{version}/filestore/powerpack/{pathv1}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilestoreSystemPatchWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available patch file resources.</td>
    <td style="padding:15px">{base_path}/{version}/filestore/system_patch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilestoreSystemPatchXWithQuery(x, query, callback)</td>
    <td style="padding:15px">Download a patch file.</td>
    <td style="padding:15px">{base_path}/{version}/filestore/system_patch/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getFilestoreSystemPatchXInfoWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the information associated with a patch file.</td>
    <td style="padding:15px">{base_path}/{version}/filestore/system_patch/{pathv1}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInterface(body, callback)</td>
    <td style="padding:15px">Add an interface record to a device.</td>
    <td style="padding:15px">{base_path}/{version}/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an interface.</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInterfaceX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of an interface.</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putInterfaceX(x, body, callback)</td>
    <td style="padding:15px">Replace an interface record.</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterfaceX(x, callback)</td>
    <td style="padding:15px">Delete an interface record.</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceXInterfaceDataDataWithQuery(x, query, callback)</td>
    <td style="padding:15px">View data for an interface.</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}/interface_data/data?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceXInterfaceDataNormalizedDailyWithQuery(x, query, callback)</td>
    <td style="padding:15px">View daily normalized data for an interface.</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}/interface_data/normalized_daily?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceXInterfaceDataNormalizedHourlyWithQuery(x, query, callback)</td>
    <td style="padding:15px">View hourly normalized data for an interface.</td>
    <td style="padding:15px">{base_path}/{version}/interface/{pathv1}/interface_data/normalized_hourly?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceMetricWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of interface metrics.</td>
    <td style="padding:15px">{base_path}/{version}/interface_metric?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceMetricXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about an interface metric.</td>
    <td style="padding:15px">{base_path}/{version}/interface_metric/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceTagWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of interface tags and their names.</td>
    <td style="padding:15px">{base_path}/{version}/interface_tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInterfaceTag(body, callback)</td>
    <td style="padding:15px">Add a new interface tag.</td>
    <td style="padding:15px">{base_path}/{version}/interface_tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putInterfaceTag(body, callback)</td>
    <td style="padding:15px">Update the name of an interface tag.</td>
    <td style="padding:15px">{base_path}/{version}/interface_tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteInterfaceTag(callback)</td>
    <td style="padding:15px">Delete an interface tag. You cannot delete a tag mapped to an interface.</td>
    <td style="padding:15px">{base_path}/{version}/interface_tag?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available monitoring policy resources.</td>
    <td style="padding:15px">{base_path}/{version}/monitor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorCvWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of web content monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/cv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorCv(body, callback)</td>
    <td style="padding:15px">Create a new web content monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/cv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorCvXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a web content monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/cv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorCvX(x, body, callback)</td>
    <td style="padding:15px">Update a web content monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/cv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitorCvX(x, body, callback)</td>
    <td style="padding:15px">Replace a web content monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/cv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitorCvX(x, callback)</td>
    <td style="padding:15px">Delete a web content monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/cv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorDnsWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of domain name monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorDns(body, callback)</td>
    <td style="padding:15px">Create a new domain name monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorDnsXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a domain name monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorDnsX(x, body, callback)</td>
    <td style="padding:15px">Update a domain name monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitorDnsX(x, body, callback)</td>
    <td style="padding:15px">Replace a domain name monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitorDnsX(x, callback)</td>
    <td style="padding:15px">Delete a domain name monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorEmailWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Email round-trip monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorEmail(body, callback)</td>
    <td style="padding:15px">Create a new Email round-trip monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorEmailXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View an Email round-trip monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/email/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorEmailX(x, body, callback)</td>
    <td style="padding:15px">Update an Email round-trip monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/email/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitorEmailX(x, body, callback)</td>
    <td style="padding:15px">Replace an Email round-trip monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/email/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitorEmailX(x, callback)</td>
    <td style="padding:15px">Delete an Email round-trip monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/email/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorPortWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of port monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorPort(body, callback)</td>
    <td style="padding:15px">Create a new port monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorPortXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a port monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorPortX(x, body, callback)</td>
    <td style="padding:15px">Update a port monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitorPortX(x, body, callback)</td>
    <td style="padding:15px">Replace a port monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitorPortX(x, callback)</td>
    <td style="padding:15px">Delete a port monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/port/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorProcess(body, callback)</td>
    <td style="padding:15px">Create a new system process monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/process?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorProcessWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of system process monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/process?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorProcessXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a system process monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/process/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorProcessX(x, body, callback)</td>
    <td style="padding:15px">Update a system process monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/process/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitorProcessX(x, body, callback)</td>
    <td style="padding:15px">Replace a system process monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/process/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitorProcessX(x, callback)</td>
    <td style="padding:15px">Delete a system process monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/process/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorServiceWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Windows service monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorService(body, callback)</td>
    <td style="padding:15px">Create a new Windows service monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorServiceXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a Windows service monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorServiceX(x, body, callback)</td>
    <td style="padding:15px">Update a Windows service monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitorServiceX(x, body, callback)</td>
    <td style="padding:15px">Replace a Windows service monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitorServiceX(x, callback)</td>
    <td style="padding:15px">Delete a Windows service monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/service/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorTvWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of SOAP/XML transaction monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/tv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorTv(body, callback)</td>
    <td style="padding:15px">Create a new SOAP/XML transaction monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/tv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMonitorTvXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a SOAP/XML transaction monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/tv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postMonitorTvX(x, body, callback)</td>
    <td style="padding:15px">Update a SOAP/XML transaction monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/tv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putMonitorTvX(x, body, callback)</td>
    <td style="padding:15px">Replace a SOAP/XML transaction monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/tv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteMonitorTvX(x, callback)</td>
    <td style="padding:15px">Delete a SOAP/XML transaction monitoring policy.</td>
    <td style="padding:15px">{base_path}/{version}/monitor/tv/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of organizations.</td>
    <td style="padding:15px">{base_path}/{version}/organization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganization(body, callback)</td>
    <td style="padding:15px">Create an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganizationX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOrganizationX(x, body, callback)</td>
    <td style="padding:15px">Replace an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationX(x, callback)</td>
    <td style="padding:15px">Delete an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXLogWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of logs associated with an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/log/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXLogYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a log message associated with an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/log/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXNoteWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of notes associated with an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganizationXNote(x, body, callback)</td>
    <td style="padding:15px">Add a note to an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXNoteYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a note associated with an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postOrganizationXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Update a note associated with an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOrganizationXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a note associated with an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOrganizationXNoteY(x, y, callback)</td>
    <td style="padding:15px">Delete a note associated with an organization.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXNoteYMediaWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of files associated with an organization note.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}/media?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXNoteYMediaZWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">Get a media file associated with an organization note.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putOrganizationXNoteYMediaZ(x, y, z, body, callback)</td>
    <td style="padding:15px">Add a media file to an organization note.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOrganizationXNoteYMediaZInfoWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">View meta-data about a media file associated with an organization note.</td>
    <td style="padding:15px">{base_path}/{version}/organization/{pathv1}/note/{pathv2}/media/{pathv3}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available performance data resources.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available performance data resources for devices.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceAvailWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data about availability and latency.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/avail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceDynamicAppWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from one or more Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/dynamic_app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceFilesystemWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from file system usage policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/filesystem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceMonitorCvWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from web content monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/monitor_cv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceMonitorDnsWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from DNS monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/monitor_dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceMonitorEmailWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from email round-trip monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/monitor_email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceMonitorPortWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from a port monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/monitor_port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceMonitorProcessWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from system process monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/monitor_process?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceMonitorServiceWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from Windows service monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/monitor_service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceDeviceMonitorTvWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data from SOAP/XML transaction monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/device/monitor_tv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceInterfaceWithQuery(query, callback)</td>
    <td style="padding:15px">View normalized (rolled-up) data about CBQoS objects.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available raw performance data resources.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available raw performance data resources for devices.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceAvailWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data about availability and latency.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/avail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceDynamicAppWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from one or more Dynamic Applications.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/dynamic_app?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceFilesystemWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from file system usage policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/filesystem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceMonitorCvWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from web content monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/monitor_cv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceMonitorDnsWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from DNS monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/monitor_dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceMonitorEmailWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from email round-trip monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/monitor_email?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceMonitorPortWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from a port monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/monitor_port?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceMonitorProcessWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from system process monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/monitor_process?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceMonitorServiceWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from Windows service monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/monitor_service?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawDeviceMonitorTvWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data from SOAP/XML transaction monitoring policies.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/device/monitor_tv?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawInterfaceWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data about interface utilization.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDataPerformanceRawCbqosWithQuery(query, callback)</td>
    <td style="padding:15px">View raw data about CBQoS objects.</td>
    <td style="padding:15px">{base_path}/{version}/data_performance_raw/cbqos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPowerpackWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of PowerPacks.</td>
    <td style="padding:15px">{base_path}/{version}/powerpack?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPowerpackXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a PowerPack.</td>
    <td style="padding:15px">{base_path}/{version}/powerpack/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProductWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of Product SKUs.</td>
    <td style="padding:15px">{base_path}/{version}/product?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProduct(body, callback)</td>
    <td style="padding:15px">Create a new Product SKU.</td>
    <td style="padding:15px">{base_path}/{version}/product?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProductXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a Product SKU.</td>
    <td style="padding:15px">{base_path}/{version}/product/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postProductX(x, body, callback)</td>
    <td style="padding:15px">Update a Product SKU.</td>
    <td style="padding:15px">{base_path}/{version}/product/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putProductX(x, body, callback)</td>
    <td style="padding:15px">Replace a Product SKU.</td>
    <td style="padding:15px">{base_path}/{version}/product/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteProductX(x, callback)</td>
    <td style="padding:15px">Delete a Product SKU.</td>
    <td style="padding:15px">{base_path}/{version}/product/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScaleWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of scale values associated with metrics.</td>
    <td style="padding:15px">{base_path}/{version}/scale?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScaleXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View detials about a scale value associated with metrics.</td>
    <td style="padding:15px">{base_path}/{version}/scale/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleWithQuery(query, callback)</td>
    <td style="padding:15px">View a list of schedules.</td>
    <td style="padding:15px">{base_path}/{version}/schedule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSchedule(body, callback)</td>
    <td style="padding:15px">Create a new schedule.</td>
    <td style="padding:15px">{base_path}/{version}/schedule/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a schedule.</td>
    <td style="padding:15px">{base_path}/{version}/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postScheduleX(x, body, callback)</td>
    <td style="padding:15px">Update a schedule.</td>
    <td style="padding:15px">{base_path}/{version}/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteScheduleX(x, callback)</td>
    <td style="padding:15px">Delete a schedule.</td>
    <td style="padding:15px">{base_path}/{version}/schedule/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleXTaskYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a list of tasks aligned to the schedule.</td>
    <td style="padding:15px">{base_path}/{version}/schedule/{pathv1}/task/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of patches registered in the system.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View information about a registered patch.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchXLogWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of log messages from the last execution of a patch.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch/{pathv1}/log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchXLogYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a log message from the last execution of a patch.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch/{pathv1}/log/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchStageWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of staged patches.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch_stage?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchStageXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View information about a staged patch.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch_stage/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchDeployActiveWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of patches currently being installed.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch_deploy_active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemPatchDeployActiveXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View information about a patch that is currently being installed.</td>
    <td style="padding:15px">{base_path}/{version}/system_patch_deploy_active/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemSettingsWithQuery(query, callback)</td>
    <td style="padding:15px">View the index of available system settings resources.</td>
    <td style="padding:15px">{base_path}/{version}/system_settings?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemSettingsSystemThresholdsWithQuery(query, callback)</td>
    <td style="padding:15px">View the global threshold settings.</td>
    <td style="padding:15px">{base_path}/{version}/system_settings/system_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemSettingsSystemThresholds(body, callback)</td>
    <td style="padding:15px">Update the global threshold settings.</td>
    <td style="padding:15px">{base_path}/{version}/system_settings/system_thresholds?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemThresholdWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of system-level thresholds for metrics associated with interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/system_threshold?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemThresholdXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a system-level threshold for a metric associated with interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/system_threshold/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postSystemThresholdX(x, body, callback)</td>
    <td style="padding:15px">Update the settings for a system-level interface metric threshold.</td>
    <td style="padding:15px">{base_path}/{version}/system_threshold/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskWithQuery(query, callback)</td>
    <td style="padding:15px">View a list of tasks. A task is any item that can be scheduled, such as a discovery session.</td>
    <td style="padding:15px">{base_path}/{version}/task/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTask(body, callback)</td>
    <td style="padding:15px">Create a new task.</td>
    <td style="padding:15px">{base_path}/{version}/task/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a task.</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTaskX(x, body, callback)</td>
    <td style="padding:15px">Update a task.</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTaskX(x, callback)</td>
    <td style="padding:15px">Delete a task.</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTaskXScheduleYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a list of schedules aligned to the task.</td>
    <td style="padding:15px">{base_path}/{version}/task/{pathv1}/schedule/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThemeWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of themes.</td>
    <td style="padding:15px">{base_path}/{version}/theme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTheme(body, callback)</td>
    <td style="padding:15px">Create a new theme.</td>
    <td style="padding:15px">{base_path}/{version}/theme?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThemeXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a theme.</td>
    <td style="padding:15px">{base_path}/{version}/theme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postThemeX(x, body, callback)</td>
    <td style="padding:15px">Update a theme.</td>
    <td style="padding:15px">{base_path}/{version}/theme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putThemeX(x, body, callback)</td>
    <td style="padding:15px">Replace a theme.</td>
    <td style="padding:15px">{base_path}/{version}/theme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThemeX(x, callback)</td>
    <td style="padding:15px">Delete a theme.</td>
    <td style="padding:15px">{base_path}/{version}/theme/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdValueOverrideWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of threshold overrides that are in place for metrics associated with in</td>
    <td style="padding:15px">{base_path}/{version}/threshold_value_override?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postThresholdValueOverride(body, callback)</td>
    <td style="padding:15px">Add a threshold override for a metric on an interface.</td>
    <td style="padding:15px">{base_path}/{version}/threshold_value_override?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThresholdValueOverrideXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about a threshold override for a metric associated with a specific interface.</td>
    <td style="padding:15px">{base_path}/{version}/threshold_value_override/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postThresholdValueOverrideX(x, body, callback)</td>
    <td style="padding:15px">Update a threshold override for a metric associated with a specific interface.</td>
    <td style="padding:15px">{base_path}/{version}/threshold_value_override/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putThresholdValueOverrideX(x, body, callback)</td>
    <td style="padding:15px">Replace a threshold override for a metric associated with a specific interface.</td>
    <td style="padding:15px">{base_path}/{version}/threshold_value_override/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteThresholdValueOverrideX(x, callback)</td>
    <td style="padding:15px">Remove a threshold override for a metric associated with a specific interface.</td>
    <td style="padding:15px">{base_path}/{version}/threshold_value_override/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of tickets.</td>
    <td style="padding:15px">{base_path}/{version}/ticket?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicket(body, callback)</td>
    <td style="padding:15px">Create a new ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketX(x, body, callback)</td>
    <td style="padding:15px">Replace a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketX(x, body, callback)</td>
    <td style="padding:15px">Update a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXLogWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of logs associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/log/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXLogYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a log message associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/log/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXNoteWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of notes associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXNote(x, body, callback)</td>
    <td style="padding:15px">Add a note to a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXNoteYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a note associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Update a note associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a note associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXNoteYMediaWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of files associated with a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/{pathv2}/media?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXNoteYMediaZWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">Get a media file associated with a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketXNoteYMediaZ(x, y, z, body, callback)</td>
    <td style="padding:15px">Add a media file to a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXNoteYMediaZInfoWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">View meta-data about a media file associated with a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/note/{pathv2}/media/{pathv3}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXWatcherExtWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of external watchers associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_ext?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXWatcherExt(x, body, callback)</td>
    <td style="padding:15px">Add an external watcher to a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_ext?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXWatcherExtYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View an external watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_ext/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXWatcherExtY(x, y, body, callback)</td>
    <td style="padding:15px">Update an external watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_ext/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketXWatcherExtY(x, y, body, callback)</td>
    <td style="padding:15px">Replace an external watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_ext/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTicketXWatcherExtY(x, y, callback)</td>
    <td style="padding:15px">Remove an external watcher from a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_ext/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXWatcherOrgWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of organization watchers associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_org?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXWatcherOrg(x, body, callback)</td>
    <td style="padding:15px">Add an organization watcher to a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_org?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXWatcherOrgYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View an organization watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_org/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXWatcherOrgY(x, y, body, callback)</td>
    <td style="padding:15px">Update an organization watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_org/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketXWatcherOrgY(x, y, body, callback)</td>
    <td style="padding:15px">Replace an organization watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_org/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTicketXWatcherOrgY(x, y, callback)</td>
    <td style="padding:15px">Remove an organization watcher from a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_org/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXWatcherQueueWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of ticket queue watchers associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_queue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXWatcherQueue(x, body, callback)</td>
    <td style="padding:15px">Add a ticket queue watcher to a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_queue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketXWatcherQueueYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a ticket queue watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_queue/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketXWatcherQueueY(x, y, body, callback)</td>
    <td style="padding:15px">Update a ticket queue watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_queue/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketXWatcherQueueY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a ticket queue watcher associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_queue/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTicketXWatcherQueueY(x, y, callback)</td>
    <td style="padding:15px">Remove a ticket queue watcher from a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket/{pathv1}/watcher_queue/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketCategoryWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of ticket categories.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_category?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketCategoryXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a ticket category.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_category/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketChargebackWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of ticket chargeback entries.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_chargeback?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketChargebackXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a ticket chargeback entry.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_chargeback/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketLogWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of all ticket logs.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_log?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketLogXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a log message associated with a ticket.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_log/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketNoteWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of all ticket notes.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketNoteXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketNoteX(x, body, callback)</td>
    <td style="padding:15px">Update a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketNoteX(x, body, callback)</td>
    <td style="padding:15px">Replace a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketNoteXMediaWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of files associated with a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note/{pathv1}/media?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketNoteXMediaYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">Get a media file associated with a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note/{pathv1}/media/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketNoteXMediaY(x, y, body, callback)</td>
    <td style="padding:15px">Add a media file to a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note/{pathv1}/media/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketNoteXMediaYInfoWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View meta-data about a media file associated with a ticket note.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_note/{pathv1}/media/{pathv2}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketQueueWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of ticket queues.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_queue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketQueue(body, callback)</td>
    <td style="padding:15px">Create a new ticket queue.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_queue?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketQueueXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a ticket queue.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_queue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketQueueX(x, body, callback)</td>
    <td style="padding:15px">Update a ticket queue.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_queue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketQueueX(x, body, callback)</td>
    <td style="padding:15px">Replace a ticket queue.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_queue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTicketQueueX(x, callback)</td>
    <td style="padding:15px">Delete a ticket queue.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_queue/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketStateWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of ticket states.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketState(body, callback)</td>
    <td style="padding:15px">Create a new ticket state.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTicketStateXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a ticket state.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTicketStateX(x, body, callback)</td>
    <td style="padding:15px">Update a ticket state.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putTicketStateX(x, body, callback)</td>
    <td style="padding:15px">Replace a ticket state.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTicketStateX(x, callback)</td>
    <td style="padding:15px">Delete a ticket state.</td>
    <td style="padding:15px">{base_path}/{version}/ticket_state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnitWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of unit values associated with metrics.</td>
    <td style="padding:15px">{base_path}/{version}/unit?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUnitXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View details about a unit value associated with metrics.</td>
    <td style="padding:15px">{base_path}/{version}/unit/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountPolicyWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of user policies.</td>
    <td style="padding:15px">{base_path}/{version}/account_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountPolicy(body, callback)</td>
    <td style="padding:15px">Create a new user policy.</td>
    <td style="padding:15px">{base_path}/{version}/account_policy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAccountPolicyXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View the properties of a user policy.</td>
    <td style="padding:15px">{base_path}/{version}/account_policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAccountPolicyX(x, body, callback)</td>
    <td style="padding:15px">Update the properties of a user policy.</td>
    <td style="padding:15px">{base_path}/{version}/account_policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putAccountPolicyX(x, body, callback)</td>
    <td style="padding:15px">Replace a user policy.</td>
    <td style="padding:15px">{base_path}/{version}/account_policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteAccountPolicyX(x, callback)</td>
    <td style="padding:15px">Delete a user policy.</td>
    <td style="padding:15px">{base_path}/{version}/account_policy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorWithQuery(query, callback)</td>
    <td style="padding:15px">View/search/filter the list of vendor records.</td>
    <td style="padding:15px">{base_path}/{version}/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVendor(body, callback)</td>
    <td style="padding:15px">Create a new vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorXWithQuery(x, query, callback)</td>
    <td style="padding:15px">View a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVendorX(x, body, callback)</td>
    <td style="padding:15px">Update a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVendorX(x, body, callback)</td>
    <td style="padding:15px">Replace a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteVendorX(x, callback)</td>
    <td style="padding:15px">Delete a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorXNoteWithQuery(x, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of notes associated with a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVendorXNote(x, body, callback)</td>
    <td style="padding:15px">Add a note to a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorXNoteYWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View a note associated with a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postVendorXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Update a note associated with a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVendorXNoteY(x, y, body, callback)</td>
    <td style="padding:15px">Replace a note associated with a vendor record.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorXNoteYMediaWithQuery(x, y, query, callback)</td>
    <td style="padding:15px">View/search/filter the list of files associated with a vendor record note.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note/{pathv2}/media?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorXNoteYMediaZWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">Get a media file associated with a vendor record note.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putVendorXNoteYMediaZ(x, y, z, body, callback)</td>
    <td style="padding:15px">Add a media file to a vendor record note.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note/{pathv2}/media/{pathv3}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getVendorXNoteYMediaZInfoWithQuery(x, y, z, query, callback)</td>
    <td style="padding:15px">View meta-data about a media file associated with a vendor record note.</td>
    <td style="padding:15px">{base_path}/{version}/vendor/{pathv1}/note/{pathv2}/media/{pathv3}/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
